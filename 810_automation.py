#!/usr/bin/python

# Auteur : Maximo Rose
# Website : https://maximorose.eu
# Organisation : 810
# Org.'s website : https://810.fr 
#---------------------------------------------------------------------
# Page creation automation for http://hchpmn.net
#---------------------------------------------------------------------
# EN : import librairies to call os functions like browsing and coying files // source : https://stackoverflow.com/questions/3207219/how-do-i-list-all-files-of-a-directory
# FR : import des librairies necessaires au parcours des fichiers et dossiers sur l'ordinateur
from curses import init_pair
from distutils.log import error
from nis import cat
import os
import shutil
# import sys
from warnings import catch_warnings
# import librairies to resize images
# Import des librairies pour redimensionner les images
from PIL import Image

#####################################################################################
#
# PRELIMINARY FUNCTIONS / IN THIS PART, WE DEFINE FUNCTIONS THAT DO A SPECIFIC TASK
# each of these will later be called, directly or indirectly in the main function 
# which is ran every time you call 810_automation.py from the terminal.
#
# To understand the code better, it's often easier to start by reading the main function
# Though, for the one who writes it, it's clearer to have functions defined before they are called (LOGIC !)
#
#####################################################################################
# EN : function to dump text content in file (needed to create HTML pages)
# FR : fonction pour dumper un contenu textuel dans un fichier (necessaire pour creer les pages HTML)
def dump_txt_in_file(filename = '', outputfolder = "", txt = ''):
    nomjson = outputfolder + filename
    with open(nomjson, 'w') as myfile:
        myfile.write(txt)
    return

# EN : Function to create the list of pages. Needed to create an updated version of the javascript
# FR : Fonction pour creer une liste de pages a referencer dans le fichier javascript execute sur clic
def create_list_of_pagenames(pagenameinteger) : 
    cnt = 0
    list_of_pagenames = []
    while(cnt <= pagenameinteger+1) :
        hexcnt = hex(cnt)
        pagename = hexcnt[2:len(hexcnt)]
        pagename = pagename.upper()
        list_of_pagenames.append(pagename+".html")
        cnt += 1
    return list_of_pagenames

# EN : Function that creates the javascript text (not the file). The paramater "pagenameinteger" is used to tell the number of HTML pages that has to be referenced
# FR : Fonction qui genere le texte du Javascript. Le parametre "pagenameinteger" indique le nombre de page HTML a referencer
def create_javascript(pagenameinteger) :
    prefix_string = "var sites = "
    list_of_pagenames = create_list_of_pagenames(pagenameinteger)
    list_of_pagenames.append("list.html")
    sufix_string = ";function randomSite() {var i = parseInt(Math.random() * sites.length);location.href = sites[i];}"
    javascript_text = prefix_string + str(list_of_pagenames) + sufix_string
    return javascript_text


# EN 
# Create a new webpage based on a photo name
# prefix string : your page code before the image name
# - "\" character are needed a the end of the line to tell python : next line is still in the variable.
# - "\" character are needed before " to tell python : the character is part of the variable, not the end of it
def create_webpage(imagename, outputfolder) :
    prefix_string = "<!DOCTYPE html> \
                    <html> \
                    <head> \
                    <link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> \
                    <script src=\"script.js\"></script> \
                    </head> \
                    <a href=\"#\" onclick=\"randomSite();\"> \
                    <body> \
                    <p> \
                    <div id=\"main\"> \
                    <img src=\""
    # rest of code after the page name
    suffix_string = ".jpg\"></br> \
                    </body> \
                    </div> \
                    </a> \
                    </html>"
    # concatenate all three element to have the complete text of a webpage
    html_txt = prefix_string + imagename + suffix_string
    # create the associated webpage
    dump_txt_in_file(filename=imagename.upper() + '.html', outputfolder=outputfolder, txt=html_txt)

    return html_txt

# Get number of images and webpages to create
def get_number_of_newimages (srcdir) : 
    directory = os.fsencode(srcdir)
    cnt = 0
    for file in os.listdir(directory):
        cnt += 1
    return cnt

# Resize images 
# algo : check which is bigger between height and width
# Set the bigger one to 800 px
# Scale the image accordingly
def resize_image(srcimage_path, destoutputname) :
    try :
        current_image = Image.open(srcimage_path)

        # Size of the image in pixels (size of original image)
        # (This is not mandatory)
        width, height = current_image.size
        fixed_size = 800

        if width > height :
            # EN : We fix the width to 800 px
            # FR : On fixe la largeur a 800 px
            width_size = fixed_size
            # EN : We evaluate the transformation rate
            # FR : On evalue le taux
            width_percent = (width_size/width)
            # EN : We apply that rate to the height
            # FR : On applique le meme taux de transformation a la hauteur
            height_size = int(height*float(width_percent))
            # EN : We resize the image
            # FR : On redimensionne l'image
            current_image = current_image.resize((width_size, height_size), Image.NEAREST)
            # EN : We save it with the appropriate name
            # FR : On sauvegarde avec le nom mis a jour.
            current_image.save(destoutputname)
        else :
            # EN : We fix the height to 800 px
            # FR : On fixe la hauteur a 800 px
            height_size = fixed_size
            # EN : We evaluate the transformation rate
            # FR : On evalue le taux
            height_percent = (height_size/height)
            # EN : We apply that rate to the width
            # FR : On applique le meme taux de transformation a la largeur
            width_size = int(width*float(height_percent))
            # EN : We resize the image
            # FR : On redimensionne l'image
            current_image = current_image.resize((width_size, height_size), Image.NEAREST)
            # EN : We save it with the appropriate name
            # FR : On sauvegarde avec le nom mis a jour.
            current_image.save(destoutputname)
    except :
        print()
        print("error processing image " + srcimage_path)
        print()
    return

# Create all the corresponding images in the output folder (renamed and resized)
def create_new_images (srcdir, destdir, int_firstname, rsize) : 
    directory = os.fsencode(srcdir)
    currentnumber = int_firstname


    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if not filename.endswith(".jpg") and not filename.endswith(".JPG") : 
            print(filename + " is not a .jpg")
            continue
        else:
            
            new_filename_hex = hex(currentnumber)
            new_filename = new_filename_hex[2:len(new_filename_hex)]

            # 810 MBR mod, 25/7/22 :
            # Added a "noresize" argument, in case images should not be
            if rsize :
                resize_image(srcimage_path= srcdir + filename, destoutputname= destdir + new_filename.upper() + ".jpg")
            else :
                current_image = Image.open(srcdir + filename)
                current_image.save(destdir + new_filename.upper() + ".jpg")

            currentnumber += 1
            continue

# Get list of all image names in the output folder to create all the associated webpages
def get_list_of_output_images (dir) : 
    directory = os.fsencode(dir)
    list_of_image_names = []
    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if not filename.endswith(".jpg") and not filename.endswith(".JPG") : 
            # If you want to make sure, files are considered, uncomment the following line
            # print(filename + " is not a .jpg")
            continue
        else :
            list_of_image_names.append(filename.split('.')[0])

    return list_of_image_names


##########################################################################
#
# MAIN FUNCTION WHICH CALLS THE ONES DEFINED PREVIOUSLY
#
##########################################################################
# VARIABLE INITIALIZATION
# variable containing the path to the output folder, relatively to the current path (path from which the script is launched)
ouputfolder_relativepath = "./output_assets/"
# variable containing the path to the input image folder, relatively to the current path (path from which the script is launched)
inputimagesfolder_relativepath = "./input_images/"
# name of the javascript file to create
javascript_name = "script.js"
# Get all the arguments passed when running command "python 810_automation.py [argv]"
# number_of_arguments = len(sys.argv)
# variable containing the last page name so far (initialized to a default value)
last_page_name = "1"
# Rezize variable
resize_wanted = True
getout = False

# FILE GENERATION USING PREVIOUSLY DEFINED FUNCTIONS
print("EN : Starting automation...")

# 810 MBR 25/7/22 modification
# EN : Get the arguments passed when the file was called from the terminal
# if (number_of_arguments > 1) :
#     last_page_name = sys.argv[1]
#     print("Argument passed. initial page value : " + str(last_page_name))
#     # if (number_of_arguments > 2) :
#     #     if(sys.argv[2] == "noresize") :
#     #         resize_wanted = False
# else :
#     print("No argument passed. Using default initial page value : 1")

while not getout :
    print()
    last_page_name = input("Please enter the last page number on your website : " )
    if(last_page_name == "") :
        last_page_name = "1"
        getout = True
        print("Using 1 as default.")
    else :
        try :
            int(last_page_name, 16) + 1
            getout = True
        except : 
            print("Could not convert into hexa, please retry.")
            getout = False

getout = False

while not getout :
    print()
    rsize_answer = input("Do you want to resize the images (800px max) ? (yes/no)    " )
    if(rsize_answer.upper() == "YES") :
        resize_wanted = True
        getout = True
    elif(rsize_answer.upper() == "NO") :
        resize_wanted = False
        getout = True
    else :
        print("Please use 'yes' or 'no' only. Be binary.")


# EN : Based on the last page name (passed as a terminal argument), intialize nextpage number, which will later be converted to hexa
next_pagename_integer = int(last_page_name, 16) + 1


# Start analyzing image folder to evaluate last page name that will be created
# more infos : https://stackoverflow.com/questions/10377998/how-can-i-iterate-over-files-in-a-given-directory 
print()
num_of_new_images = get_number_of_newimages(srcdir = inputimagesfolder_relativepath)
print("Number of webpages to create and image to transform : " + str(num_of_new_images))
next_lastpage_name_int = int(last_page_name, 16) + num_of_new_images
next_lastpage_name_hex = hex(next_lastpage_name_int)
next_lastpage_name = next_lastpage_name_hex[2:len(next_lastpage_name_hex)]
print("Last page name will be : " + str(next_lastpage_name))
print()

# Create all the new images and name them based on the initial starting number
print("Creating images...")
create_new_images(srcdir= inputimagesfolder_relativepath, destdir= ouputfolder_relativepath, int_firstname=next_pagename_integer, rsize = resize_wanted)
print("All the new images were created.")
list_op_img = get_list_of_output_images(dir=ouputfolder_relativepath)
print()

# Create all the new webpages and name them based on : 
# 1. the initial starting number 
# 2. all the images in the "output_assets" folder
print("Creating webpages...")
for img in list_op_img :
    create_webpage(img, outputfolder = ouputfolder_relativepath) 
print("All the new webpages were created.")
print()

# Create the updated javascript with the new webpages to reference.
print("Creating javascript...")
# Create text content of javascript
js_text = create_javascript(pagenameinteger=next_lastpage_name_int)
# Create javascript file
dump_txt_in_file(filename=javascript_name, outputfolder=ouputfolder_relativepath, txt=js_text)
print("The new javascript was created.")
print()
print("Check the output_assets folder.")
print()




