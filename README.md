# hchpmn.net // 810 automation

# 🇬🇧 EN
## Project description
Page creation automation for hchpmn.net.

hchpmn.net displays a new picture, in a new webpage, every time you click on the page.
Webpages and pictures are named in an incremental way, using hexadecimal.
So, everytime they want to add a new collection of photos, they :
1. Resize the picture and rename it with the appropriatie hexa reference ;
2. Create a webpage referencing the newly created image ;
3. Update the javascript launched on the click event on the page, so that it references all the webpages, including the new ones.

The script in the current repository will simply automate the process by : 
1. Listing all the pictures put in the input_images folder ;
2. Resize and rename each of these images to save them in the output_assets folder ;
3. Create all the webpages associated to the recently created images ;
4. Create a javascript "script.js" which updates the list of pages to chose from randomly when the mouse is clicked. 

If one wants to change the HTML webpage base, or the javascript base, one needs to modify the python code 810_automation.py has they are hard coded in function "create_webpage(imagename, outputfolder)" and "create_javascript(pagenameinteger)".

## How to use it

### Set up your running environment.
- Install the latest version of Python.
- Download the current source code from Gitlab
- Move the downloaded zip file in the folder you plan to run it in.
- Extract everything

### Set up the input images.
- Copy/Paste all your new pictures in the "input_images" folder you've extracted
- On your webserver, get the reference number of the last image created. The script will ask you for it (default is "1")

### Run the python script.
Piece of advice : Clean the "output_assets" folder before running the script. Everything will be clearer.
- Open a Terminal window
- Go in the folder where "810_automation.py" script is, using the "cd " command
- Type the command : "python 810_automation.py"

The script will ask you for the last page number.
Then, you wil be asked if you want to resize the images (800px max)

### Update your websites with the new assets 
- Upload all the files in the output_assets folder to your webserver, overwriting the existing ones (back the old ones up by renaming them before, for the first use)


## How to edit it
### Dependancies
- conda
- os
- shutil
- PIL

### IDE
I personnaly use Microsoft Visual Code to edit python files, and launch the script from the IDE's Terminal.
